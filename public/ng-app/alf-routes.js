'use strict';

app.config(function($routeProvider){
	$routeProvider
		.when("/dashboard-configurations", {
			templateUrl: "/ng-app/configurations-list/cfg-list.html",
			controller: "cfgListCtrl"
		})
		.when("/repository", {
			templateUrl: "/ng-app/repository-view/repo-list-view.html",
			controller: "repoViewCtrl"
		})
		.otherwise({
			templateUrl: "/ng-app/defaultTemplate.html"
		});
});

