'use strict';

app.controller("appController", function($scope, $http){
	var cmis = "http://k1072-02:8081/alfresco/api/-default-/public/cmis/versions/1.1/browser/root/sites/surf-config/components?maxItems=100";
	var content = "http://k1072-02:8081/alfresco/api/-default-/public/cmis/versions/1.1/atom/content?id=";
	var postQuery = "http://k1072-02:8081/alfresco/api/-default-/public/cmis/versions/1.1/browser";
	
	$scope.folders = [];

	$scope.queryResult = [];
	
	$scope.user = {
		username: 'admin',
		password: 'Kurwamac!!'
	};
	$scope.filename ='';
	$scope.fileContent = {};
	$scope.cmisEndpoint = "http://k1072-02:8081/alfresco/api/-default-/public/cmis/versions/1.1/browser";
	
	$scope.onCmisSend = function(){
		console.log('on  cmis send ');
		//cmisService.index();
		var config = {
			headers: {
				"Authorization": "Basic " + btoa($scope.user.username + ":" + $scope.user.password )
			}
		};
		
		$http.get($scope.cmisEndpoint, config)
			.then(
					function success(response){
						console.log('success response --> ' + response.data );
						$scope.folders = response.data.objects;
						
						
						console.log($scope.folders[0].object.properties['cmis:name'].value);
					},
					function fail(response){
						console.log('fail response --> ' + response.data );
					}
			);
	};
	
	$scope.loadFileContent = function(item){
					var config = {
			headers: {
				"Authorization": "Basic " + btoa($scope.user.username + ":" + $scope.user.password )
			}
		};
		$http.get(content + item.object.properties['alfcmis:nodeRef'].value, config )
			.then(
				function(response){
					console.log( 'success -> ' + response.data );
					item.fileContent = response.data;
				},
				function(response){
					console.log('fail -> ' + response.data );
				}
			);
	};
	
	$scope.namedQuery = function(  ){
		var config = {
			headers: {
				"Authorization": "Basic " + btoa($scope.user.username + ":" + $scope.user.password )
			}
		};
		var data = {
			url : postQuery,
			params: {
				cmisselector: "query",
				q: $scope.selectClause
			},
			headers: {
				"Authorization": "Basic " + btoa($scope.user.username + ":" + $scope.user.password )
			}
		};
		$http.get(postQuery, data, config)
			.then(
				function(response){
					$scope.queryResult = response.data;
					console.log('success response');
				},
				function(response){
					console.log('fail response');
				}
			);
	};
});
