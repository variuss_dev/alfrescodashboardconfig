const express = require('express');
const app = express();
const path = require('path');
const root = path.join(__dirname);




app.use(express.static('public'));

app.get('/', (req,res)=>{
	console.log("serving index.html");
	res.sendFile(root + "/public/templates/index.html");
});

app.listen( 3000, () => console.log('running server on port 3000'));
